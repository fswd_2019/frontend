import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router} from "@angular/router";
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm){
    console.log(form);
    let snackBarRef = this.snackBar.open('Login correcto. Redirigiendo a la pantalla de Reservas' , null, {
      duration:3000
    });
    snackBarRef.afterDismissed().subscribe(() => {
      console.log('La snackbar se ha cerrado');
      this.router.navigate(['reservas']);
    });
  }
}
