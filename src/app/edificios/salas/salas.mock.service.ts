import { Injectable } from '@angular/core';
import { TipoSala } from '../../../api-rest/';
import {Observable} from 'rxjs';
// Añadido el observable.of y el operador delay
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

@Injectable()

export class SalasMockService {
    private tipos: TipoSala[] = [
        {"id":1, "tipo":'Auditorio'},
        {"id":2,"tipo":'Sala de reuniones'},
        {"id":3,"tipo":'Despacho'}
    ];

    getTiposSalas(){
        // Utilizamos Observable.of porque ya tenemos el tipo de datos y los datos
        return Observable.of(this.tipos).delay(3000);
    }
}