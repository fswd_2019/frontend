export * from './edificios.service';
import { EdificiosService } from './edificios.service';
export * from './provincias.service';
import { ProvinciasService } from './provincias.service';
export * from './reservas.service';
import { ReservasService } from './reservas.service';
export const APIS = [EdificiosService, ProvinciasService, ReservasService];
