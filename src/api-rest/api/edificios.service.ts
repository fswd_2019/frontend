/**
 * Practica 5
 * Definicion de una API sencilla de reserva de salas para fines educativos. Cuenta con 5 tipos principales de objeto: Usuarios, Reservas, Edificios, Salas y Recursos. 
 *
 * OpenAPI spec version: 1.0.0
 * Contact: fermatfe@jcyl.es
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { Edificio } from '../model/edificio';
import { Sala } from '../model/sala';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class EdificiosService {

    protected basePath = 'http://localhost:4220/v1';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Actualizar edificio
     * 
     * @param idEdificio ID del edificio a actualizar
     * @param edificio Nuevos Datos del edificio
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public actualizarEdificio(idEdificio: number, edificio: Edificio, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public actualizarEdificio(idEdificio: number, edificio: Edificio, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public actualizarEdificio(idEdificio: number, edificio: Edificio, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public actualizarEdificio(idEdificio: number, edificio: Edificio, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling actualizarEdificio.');
        }

        if (edificio === null || edificio === undefined) {
            throw new Error('Required parameter edificio was null or undefined when calling actualizarEdificio.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}`,
            edificio,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Actualizar sala
     * Actualiza una sala
     * @param idEdificio ID del edificio a recuperar
     * @param idSala ID de la sala a actualizar
     * @param sala Nuevos Datos de la sala
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public actualizarSala(idEdificio: number, idSala: number, sala: Sala, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public actualizarSala(idEdificio: number, idSala: number, sala: Sala, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public actualizarSala(idEdificio: number, idSala: number, sala: Sala, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public actualizarSala(idEdificio: number, idSala: number, sala: Sala, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling actualizarSala.');
        }

        if (idSala === null || idSala === undefined) {
            throw new Error('Required parameter idSala was null or undefined when calling actualizarSala.');
        }

        if (sala === null || sala === undefined) {
            throw new Error('Required parameter sala was null or undefined when calling actualizarSala.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}/salas/${encodeURIComponent(String(idSala))}`,
            sala,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Agregar edificio
     * Agrega un edificio al sistema
     * @param body Datos del nuevo edificio
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public agregarEdificio(body: Edificio, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public agregarEdificio(body: Edificio, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public agregarEdificio(body: Edificio, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public agregarEdificio(body: Edificio, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (body === null || body === undefined) {
            throw new Error('Required parameter body was null or undefined when calling agregarEdificio.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/edificios`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Agregar sala
     * Agrega una sala al sistema
     * @param idEdificio ID del edificio a recuperar
     * @param body Datos de la nueva sala
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public agregarSala(idEdificio: number, body?: Sala, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public agregarSala(idEdificio: number, body?: Sala, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public agregarSala(idEdificio: number, body?: Sala, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public agregarSala(idEdificio: number, body?: Sala, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling agregarSala.');
        }


        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}/salas`,
            body,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Borra un edificio
     * 
     * @param idEdificio ID del edificio a borrar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public borrarEdificio(idEdificio: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public borrarEdificio(idEdificio: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public borrarEdificio(idEdificio: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public borrarEdificio(idEdificio: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling borrarEdificio.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Borra una sala
     * 
     * @param idEdificio ID del edificio a recuperar
     * @param idSala ID de la sala a borrar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public borrarSala(idEdificio: number, idSala: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public borrarSala(idEdificio: number, idSala: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public borrarSala(idEdificio: number, idSala: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public borrarSala(idEdificio: number, idSala: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling borrarSala.');
        }

        if (idSala === null || idSala === undefined) {
            throw new Error('Required parameter idSala was null or undefined when calling borrarSala.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}/salas/${encodeURIComponent(String(idSala))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Busca edificio por ID
     * Devuelve un edificio por ID
     * @param idEdificio ID del edificio a recuperar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEdificioPorId(idEdificio: number, observe?: 'body', reportProgress?: boolean): Observable<Edificio>;
    public getEdificioPorId(idEdificio: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Edificio>>;
    public getEdificioPorId(idEdificio: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Edificio>>;
    public getEdificioPorId(idEdificio: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling getEdificioPorId.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Edificio>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Busca edificios
     * Busqueda de edificios
     * @param searchString Pasa una cadena opcional para buscar edificios
     * @param idProvincia Busca los edificios de una provincia\&quot;
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getEdificios(searchString?: string, idProvincia?: number, observe?: 'body', reportProgress?: boolean): Observable<Array<Edificio>>;
    public getEdificios(searchString?: string, idProvincia?: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<Edificio>>>;
    public getEdificios(searchString?: string, idProvincia?: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<Edificio>>>;
    public getEdificios(searchString?: string, idProvincia?: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {



        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (searchString !== undefined && searchString !== null) {
            queryParameters = queryParameters.set('searchString', <any>searchString);
        }
        if (idProvincia !== undefined && idProvincia !== null) {
            queryParameters = queryParameters.set('idProvincia', <any>idProvincia);
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<Edificio>>(`${this.basePath}/edificios`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Busca sala por ID
     * Devuelve una sala por ID
     * @param idEdificio ID del edificio a recuperar
     * @param idSala ID de la sala a recuperar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getSalaPorId(idEdificio: number, idSala: number, observe?: 'body', reportProgress?: boolean): Observable<Sala>;
    public getSalaPorId(idEdificio: number, idSala: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Sala>>;
    public getSalaPorId(idEdificio: number, idSala: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Sala>>;
    public getSalaPorId(idEdificio: number, idSala: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling getSalaPorId.');
        }

        if (idSala === null || idSala === undefined) {
            throw new Error('Required parameter idSala was null or undefined when calling getSalaPorId.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Sala>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}/salas/${encodeURIComponent(String(idSala))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Buscar salas
     * Busqueda de salas
     * @param idEdificio ID del edificio a recuperar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getSalas(idEdificio: number, observe?: 'body', reportProgress?: boolean): Observable<Array<Sala>>;
    public getSalas(idEdificio: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<Sala>>>;
    public getSalas(idEdificio: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<Sala>>>;
    public getSalas(idEdificio: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idEdificio === null || idEdificio === undefined) {
            throw new Error('Required parameter idEdificio was null or undefined when calling getSalas.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<Sala>>(`${this.basePath}/edificios/${encodeURIComponent(String(idEdificio))}/salas`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
