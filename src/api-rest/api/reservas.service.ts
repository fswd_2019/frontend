/**
 * Practica 5
 * Definicion de una API sencilla de reserva de salas para fines educativos. Cuenta con 5 tipos principales de objeto: Usuarios, Reservas, Edificios, Salas y Recursos. 
 *
 * OpenAPI spec version: 1.0.0
 * Contact: fermatfe@jcyl.es
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs/Observable';

import { Reserva } from '../model/reserva';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable()
export class ReservasService {

    protected basePath = 'http://localhost:4220/v1';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
            this.basePath = basePath || configuration.basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * Actualizar reserva por ID
     * 
     * @param idReserva ID de la reserva a actualizar
     * @param reserva Nuevos Datos de la reserva
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public actualizarReserva(idReserva: number, reserva: Reserva, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public actualizarReserva(idReserva: number, reserva: Reserva, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public actualizarReserva(idReserva: number, reserva: Reserva, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public actualizarReserva(idReserva: number, reserva: Reserva, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idReserva === null || idReserva === undefined) {
            throw new Error('Required parameter idReserva was null or undefined when calling actualizarReserva.');
        }

        if (reserva === null || reserva === undefined) {
            throw new Error('Required parameter reserva was null or undefined when calling actualizarReserva.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.basePath}/reservas/${encodeURIComponent(String(idReserva))}`,
            reserva,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Agregar reserva
     * Agrega una reserva al sistema\&quot;
     * @param reserva Datos de la nueva reserva
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public agregarReserva(reserva: Reserva, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public agregarReserva(reserva: Reserva, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public agregarReserva(reserva: Reserva, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public agregarReserva(reserva: Reserva, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (reserva === null || reserva === undefined) {
            throw new Error('Required parameter reserva was null or undefined when calling agregarReserva.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected != undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.basePath}/reservas`,
            reserva,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Borra una reserva por ID
     * 
     * @param idReserva ID de la reserva a borrar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public borrarReservaPorID(idReserva: number, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public borrarReservaPorID(idReserva: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public borrarReservaPorID(idReserva: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public borrarReservaPorID(idReserva: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idReserva === null || idReserva === undefined) {
            throw new Error('Required parameter idReserva was null or undefined when calling borrarReservaPorID.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.delete<any>(`${this.basePath}/reservas/${encodeURIComponent(String(idReserva))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Busca Reserva por ID
     * Devuelve una reserva por ID
     * @param idReserva ID de la reserva a recuperar
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getReservaPorId(idReserva: number, observe?: 'body', reportProgress?: boolean): Observable<Reserva>;
    public getReservaPorId(idReserva: number, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Reserva>>;
    public getReservaPorId(idReserva: number, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Reserva>>;
    public getReservaPorId(idReserva: number, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        if (idReserva === null || idReserva === undefined) {
            throw new Error('Required parameter idReserva was null or undefined when calling getReservaPorId.');
        }

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Reserva>(`${this.basePath}/reservas/${encodeURIComponent(String(idReserva))}`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * Busca reservas
     * Busqueda de reservas
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getReservas(observe?: 'body', reportProgress?: boolean): Observable<Array<Reserva>>;
    public getReservas(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<Array<Reserva>>>;
    public getReservas(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<Array<Reserva>>>;
    public getReservas(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // to determine the Accept header
        let httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected != undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<Array<Reserva>>(`${this.basePath}/reservas`,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
